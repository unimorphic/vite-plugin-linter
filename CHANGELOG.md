## 1.0.0 (2021-10-10)
* Added includeMode option
* Fixed bug where the lint output wasn't updated when a file was deleted
* Added npm vite-plugin-linter command
* Moved linting to worker threads
* Fixed bug where TypeScript lint output wasn't cleared when all errors were fixed

## 1.0.1 (2021-10-10)
* Fixed bug where new files weren't included in the TypeScript lint output

## 1.0.2 (2021-11-22)
* Fixed error when a file was renamed
* Added fancy config example

## 1.1.0 (2022-01-16)
* Added TypeScriptLinterOptions

## 1.2.0 (2022-03-14)
* Fixed file watching error on Linux

## 2.0.0 (2022-12-07)
* Updated dependencies including ESLint major version change 7 -> 8

## 2.0.2 (2022-12-21)
* Fixed TypeScriptLinter error in build mode

## 2.0.3 (2023-08-06)
* Updated dependencies

## 2.0.5 (2023-08-17)
* Fix null error

## 2.0.6 (2023-09-30)
* Fix file exists check & add read file error logging

## 2.0.7 (2023-10-06)
* Added Access-Control-Allow-Origin header & fixed npm warning

## 2.1.1 (2024-01-21)
* Added support for ESM build & updated dependencies

## 3.0.0 (2024-07-13)
* Updated dependencies including ESLint major version change 8 -> 9

## 3.0.1 (2024-11-09)
* Updated dependencies
